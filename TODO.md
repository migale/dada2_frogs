# TODO 

## High priority

- [x] add ITS management
    - [x] keep unmerged reads
    - [x] ITSx

## Low priority

- [ ] adapt fig widths to the number of samples
- [ ] add FastQC / multiQC
- [ ] nice css
- [ ] qmd instead of Rmd?
- [ ] FROGS tree?
