---
title: "`r params$expname`"
subtitle: Amplicon - "`r params$region`"
author: "`r params$author`"
date: "`r Sys.Date()`"
params:
  expname: expname
  author: author
  #metadata_file: "/work_home/orue/MICROVARIOR/RAW_DATA/BIOPROTECTION/V3F_CN/metadata.tsv"
  threads: 4
  region: 16S
  forward_primer: TACGGRAGGCWGCAG
  reverse_primer: TACCAGGGTATCTAATCCT
  input_directory: "/path/to/files"
  output_directory: "/path/to/outputs"
  reference: "/path/to/ref.fasta"
  phix: "/db/outils/FROGS/contaminants/phi.fa"
  min_reads: 1000
  min_abundance: 0.00005
  merge_min_overlap: 12
  merge_max_mismatch: 0
  its: false
  its_region: "ITS1"
  fileext: ".fastq.gz"
output:
  html_document:
    self_contained: yes
    code_folding: show
    number_sections: no
    theme: lumen
    toc: yes
    toc_float:
      collapsed: true
      smooth_scroll: true
---

<style>
  .superbigimage{overflow-x:scroll;white-space: nowrap;}
  .superbigimage img{max-width: none;}
</style>

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo =TRUE, eval=TRUE, cache = FALSE, message = FALSE, warning = FALSE, cache.lazy = FALSE,
                      fig.height = 3.5, fig.width = 8)
knitr::opts_knit$set(root.dir = '.')
options(digits=1)
```

```{r datatable global options, echo=FALSE}
options(DT.options = list(pageLength = 10,
                          language = list(search = 'Filter:'),
                          dom = 'Bfrtip',
                          scrollX = TRUE,
                          buttons = c('copy', 'csv', 'excel', 'pdf', 'print')))
```


```{r functions, echo=FALSE}
getN <- function(x) sum(getUniques(x))

get.sample.name <- function(fname) paste(strsplit(basename(fname), "_R")[[1]][1],collapse="_")

get_reads_number <- function(path){
  
  if(file.exists(file.path(path,"samples.rds"))){
    df_samples <- readRDS(file.path(path,"samples.rds"))
  }else{
    fnFs <- sort(list.files(path, pattern = "_R1.fastq.gz", full.names = TRUE)) # Extensions for R1 files
    fnRs <- sort(list.files(path, pattern = "_R2.fastq.gz", full.names = TRUE)) # Extensions for R2 files
    r1 <- as.vector(as.integer(countLines(path, pattern = "_R1.fastq.gz$")/4))
    r2 <- as.vector(as.integer(countLines(path, pattern = "_R2.fastq.gz$")/4))
    
    sample.names <- unname(sapply(fnFs, get.sample.name))
    df_samples <- data.frame(sample.names,fnFs,fnRs,r1,r2) %>% mutate(name = sample.names, R1path = fnFs, R2path = fnRs, R1 = r1, R2 = r2) %>% select(-sample.names, -fnFs, -fnRs, -r1, -r2)
    #df_samples <- df_samples_raw %>% filter(r1 > 1000) %>% mutate(name = sample.names, R1path = fnFs, R2path = fnRs, R1 = r1, R2 = r2) %>% select(-sample.names, -fnFs, -fnRs, -r1, -r2)
    saveRDS(df_samples,file.path(path,"samples.rds"))
  }
  
  return(df_samples)
}


```

We first setup our R working environment by loading a few useful packages...

```{r load-packages, message = FALSE, cache = FALSE}
library(dada2)
library(ggplot2)
library(tidyverse)
library(dada2)
library(DT)
library(ShortRead)
library(Biostrings)
library(biomformat)
library(purrr)
```

... and modify PATH to include bioinformatics tools.

```{r conda paths}
Sys.setenv(PATH = paste("/usr/local/genome/Anaconda3/envs/seqkit-2.0.0/bin", "/usr/local/genome/Anaconda3/envs/frogs-4.0.1/bin", Sys.getenv("PATH"), sep = ":"))
```

```{r, echo=FALSE}
# Define output directories
filtN_output_dir <- file.path(params$output_directory, "filtN")
cutadapt_output_dir <- file.path(params$output_dir, "cutadapt")
filters_output_dir <- file.path(params$output_dir, "filters")

```

# Quality control

## Seqkit

Fisrt, we use seqkit to obtain the number of reads by sample and the reads lengths.

```{r, eval=TRUE}
if(!file.exists(file.path(params$output_directory,"seqkit_raw.csv"))){
	system2(command="seqkit", args = c("stat",file.path(params$input_directory,paste0("*",params$fileext)), "--out-file", file.path(params$output_directory,"seqkit_raw.csv")), stdout = TRUE)
}
```

Here are the informations about the raw FASTQ files:

```{r, echo=FALSE}
raw_data <- read.csv2(file.path(params$output_directory,"seqkit_raw.csv"), stringsAsFactors = F, sep="", strip.white=T, dec=".") %>% as_tibble() %>% mutate(sample = basename(gsub('.{12}$', '', file))) %>% distinct(sample, .keep_all = TRUE) %>% select(-file)
raw_data %>% datatable()
```

```{r reads count, echo=FALSE, cache=FALSE}
df_samples_raw <- get_reads_number(params$input_directory)

p<-ggplot(data=df_samples_raw, aes(x=name, y=R1)) +
  xlab("Samples")  + ylab("Reads") +
  geom_bar(stat="identity") +
  geom_hline(yintercept = params$min_reads, linetype="dashed", color = "red") +
  theme(text = element_text(size=10),
        axis.text.x = element_text(angle=90, hjust=1)) 

```

<div class="superbigimage">
```{r plot_it , echo = FALSE, fig.width=max(8,ceiling(nrow(df_samples_raw)/2))}
p
```
</div>



## Quality profiles

Let look at the average quality profiles of R1:

```{r qc profiles r1}
fnFs <- sort(list.files(params$input_directory, pattern = "_R1.fastq.gz", full.names = TRUE)) # Extensions for R1 files
```


```{r plot qc profiles r1, eval=FALSE}
plotQualityProfile(fnFs, aggregate=TRUE)
```

and R2:

```{r qc profiles r2}
fnRs <- sort(list.files(params$input_directory, pattern = "_R2.fastq.gz", full.names = TRUE)) # Extensions for R2 files
```

```{r plot qc profiles r2, eval=FALSE}
plotQualityProfile(fnRs, aggregate=TRUE)
```

# Cleaning

## Remove reads with ambiguous bases (Ns)

Reads containing ambigous bases are removed because dada2 can not deal with them.


```{r filtN}
fnFs.filtN <- file.path(filtN_output_dir, basename(fnFs)) # Put N-filterd files in filtN/ subdirectory
fnRs.filtN <- file.path(filtN_output_dir, basename(fnRs))

if(!file.exists(file.path(filtN_output_dir,"samples.rds"))){
  filterAndTrim(fnFs, fnFs.filtN, fnRs, fnRs.filtN, maxN = 0, multithread = params$threads)
  df_samples_filtN <- get_reads_number(filtN_output_dir)
  saveRDS(df_samples_filtN,file.path(filtN_output_dir,"samples.rds"))
}else{
  df_samples_filtN <- readRDS(file.path(filtN_output_dir,"samples.rds"))
}

```

<div class="superbigimage">
```{r removeN plot, echo=FALSE, fig.width=max(8,ceiling(nrow(df_samples_raw)/2))}
df1 <- df_samples_raw %>% mutate(Raw = R1)
df2 <- df_samples_filtN %>% mutate(RemoveN = R1)
df <- full_join(df1, df2, by="name") %>% select(name, Raw, RemoveN) %>% mutate(PercLost = round(1-(RemoveN/Raw),3)*100)
df %>% datatable()
ggplot(df %>% pivot_longer(cols = c(Raw, RemoveN), names_to = "Step", values_to = "num_seqs"), 
       aes(x = name, y = num_seqs, fill=Step)) + 
  geom_bar(stat='identity', position='identity') +
  labs(y = "Reads", x = "Sample") + 
  scale_fill_brewer(palette = "Reds", direction = 1) + 
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1))
```
</div>

## Remove primers

We now remove primers (and check their orientation) from reads.

```{r param primers}
FWD <- params$forward_primer
REV <- params$reverse_primer

allOrients <- function(primer) {
    # Create all orientations of the input sequence
    require(Biostrings)
    dna <- DNAString(primer)  # The Biostrings works w/ DNAString objects rather than character vectors
    orients <- c(Forward = dna, Complement = complement(dna), Reverse = reverse(dna), 
        RevComp = reverseComplement(dna))
    return(sapply(orients, toString))  # Convert back to character vector
}
FWD.orients <- allOrients(FWD)
REV.orients <- allOrients(REV)
FWD.orients
REV.orients
```

We first check the orientation of primers in one sample.

```{r check primers}
primerHits <- function(primer, fn) {
    # Counts number of reads in which the primer is found
    nhits <- vcountPattern(primer, sread(readFastq(fn)), fixed = FALSE)
    return(sum(nhits > 0))
}
rbind(FWD.ForwardReads = sapply(FWD.orients, primerHits, fn = fnFs.filtN[[1]]), 
    FWD.ReverseReads = sapply(FWD.orients, primerHits, fn = fnRs.filtN[[1]]), 
    REV.ForwardReads = sapply(REV.orients, primerHits, fn = fnFs.filtN[[1]]), 
    REV.ReverseReads = sapply(REV.orients, primerHits, fn = fnRs.filtN[[1]]))
```

Then, cutadapt is used to remove primers

```{r cutadapt}

if(!dir.exists(cutadapt_output_dir)) dir.create(cutadapt_output_dir)
fnFs.cut <- file.path(cutadapt_output_dir, basename(fnFs))
fnRs.cut <- file.path(cutadapt_output_dir, basename(fnRs))

sample.names <- unname(sapply(fnFs, get.sample.name))

if(!file.exists(file.path(cutadapt_output_dir,"samples.rds")) && !file.exists(file.path(cutadapt_output_dir, "fnRs.cut.rds")) && !file.exists(file.path(cutadapt_output_dir, "fnRs.cut.rds"))){
  FWD.RC <- dada2:::rc(FWD)
  REV.RC <- dada2:::rc(REV)
  # Trim FWD and the reverse-complement of REV off of R1 (forward reads)
  R1.flags <- paste("-g", FWD, "-a", REV.RC) 
  # Trim REV and the reverse-complement of FWD off of R2 (reverse reads)
  R2.flags <- paste("-G", REV, "-A", FWD.RC) 
  # Run Cutadapt
  for(i in seq_along(fnFs)) {
    logfile <- system2("cutadapt", args = c(R1.flags, R2.flags, "-n", 2, # -n 2 required to remove FWD and REV from reads
                               "-o", fnFs.cut[i], "-p", fnRs.cut[i], "--discard-untrimmed", # output files
                               fnFs.filtN[i], fnRs.filtN[i]), stdout=TRUE, stderr=TRUE) # input files
    write(logfile, file=file.path(cutadapt_output_dir,paste0(sample.names[i],".cutadapt.log")))
  }
  df_samples_cutadapt <- get_reads_number(cutadapt_output_dir)
  saveRDS(df_samples_cutadapt,file.path(cutadapt_output_dir,"samples.rds"))
  saveRDS(fnFs.cut, file.path(cutadapt_output_dir, "fnFs.cut.rds"))
  saveRDS(fnRs.cut, file.path(cutadapt_output_dir, "fnRs.cut.rds"))
}else{
  df_samples_cutadapt <- readRDS(file.path(cutadapt_output_dir,"samples.rds"))
  fnFs.cut <- readRDS(file.path(cutadapt_output_dir, "fnFs.cut.rds"))
  fnRs.cut <- readRDS(file.path(cutadapt_output_dir, "fnRs.cut.rds"))
}



```

Finally, as a sanity check, we will count the presence of primers in the first cutadapt-ed sample:

```{r cutadapt check}
rbind(FWD.ForwardReads = sapply(FWD.orients, primerHits, fn = fnFs.cut[[1]]), 
    FWD.ReverseReads = sapply(FWD.orients, primerHits, fn = fnRs.cut[[1]]), 
    REV.ForwardReads = sapply(REV.orients, primerHits, fn = fnFs.cut[[1]]), 
    REV.ReverseReads = sapply(REV.orients, primerHits, fn = fnRs.cut[[1]]))
```

Here is the summary of the cutadapt process:

```{r cutadapt summary, echo=FALSE}
df1 <- df_samples_filtN %>% mutate(RemoveN = R1)
df2 <- df_samples_cutadapt %>% mutate(Cutadapt = R1)

df <- full_join(df1, df2, by="name") %>% select(name, RemoveN, Cutadapt) %>% mutate(PercLost = round(1-(Cutadapt/RemoveN),3)*100)

df %>% datatable()
my_levels <- c("RemoveN","Cutadapt")
p <- ggplot(df %>% pivot_longer(cols = c(RemoveN, Cutadapt), names_to = "Step", values_to = "num_seqs") %>% mutate(Step = factor(Step, levels = my_levels)), 
       aes(x = name, y = num_seqs, fill=Step)) + 
  geom_bar(stat='identity', position='identity') +
  labs(y = "Reads", x = "Sample") + 
  scale_fill_brewer(palette = "Reds", direction = 1) + 
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1))


```

<div class="superbigimage">
```{r cutadapt plot, echo=FALSE, fig.width=max(8,ceiling(nrow(df_samples_raw)/2))}
p
```
</div>

## Filter on length and quality

```{r filters}
fnFs.cut <- fnFs.cut[file.exists(fnFs.cut)]
fnRs.cut <- fnRs.cut[file.exists(fnRs.cut)]

filtFs <- file.path(filters_output_dir, basename(fnFs.cut))
filtRs <- file.path(filters_output_dir, basename(fnRs.cut))


if(!file.exists(file.path(filters_output_dir,"filtFs.rds")) || !file.exists(file.path(filters_output_dir, "filtRs.rds")) || !file.exists(file.path(filters_output_dir, "out.rds")) || !file.exists(file.path(filters_output_dir, "samples.rds"))){
  out <- filterAndTrim(fnFs.cut, filtFs, fnRs.cut, filtRs, maxN = 0, #maxEE = c(2, 2), 
                     truncQ = 2, minLen = 100, rm.phix = TRUE, compress = TRUE, multithread = params$threads)
  df_samples_filters <- get_reads_number(filters_output_dir)
  saveRDS(out, file.path(filters_output_dir, "out.rds"))
  saveRDS(filtFs, file.path(filters_output_dir, "filtFs.rds"))
  saveRDS(filtRs, file.path(filters_output_dir, "filtRs.rds"))
  saveRDS(df_samples_filters,file.path(filters_output_dir,"samples.rds"))
}else{
  df_samples_filters <- readRDS(file.path(filters_output_dir,"samples.rds"))
  out <- readRDS(file.path(filters_output_dir, "out.rds"))
  filtFs <- readRDS(file.path(filters_output_dir, "filtFs.rds"))
  filtRs <- readRDS(file.path(filters_output_dir, "filtRs.rds"))
}



```

Here is the summary of the filters:

```{r filters summary, echo=FALSE}
df1 <- df_samples_filters %>% mutate(Filters = R1)
df2 <- df_samples_cutadapt %>% mutate(Cutadapt = R1)
df <- full_join(df1, df2, by="name") %>% select(name, Cutadapt, Filters) %>% mutate(PercLost = round(1-(Filters/Cutadapt),3)*100)
df %>% datatable()

p <- ggplot(df %>% pivot_longer(cols = c(Cutadapt, Filters), names_to = "Step", values_to = "num_seqs"), 
       aes(x = name, y = num_seqs, fill=Step)) + 
  geom_bar(stat='identity', position='identity') +
  labs(y = "Reads", x = "Sample") + 
  scale_fill_brewer(palette = "Reds", direction = 1) + 
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1))
```

<div class="superbigimage">
```{r filters plot, echo=FALSE, fig.width=max(8,ceiling(nrow(df_samples_raw)/2))}
p
```
</div>

# Dada2

## Learn error rates

Error rates are learned by alternating between sample inference and error rate estimation until convergence.


```{r, dada2 learn error rates}
dada2_output_dir <- file.path(params$output_dir, "dada2")
if(!dir.exists(dada2_output_dir)) dir.create(dada2_output_dir)
filtFs <- filtFs[file.exists(filtFs)]
filtRs <- filtRs[file.exists(filtRs)]
if(!file.exists(file.path(dada2_output_dir,"errF.rds")) && !file.exists(file.path(dada2_output_dir,"errR.rds"))){
  errF <- learnErrors(filtFs, multithread=params$threads)
  errR <- learnErrors(filtRs, multithread=params$threads)
  saveRDS(errF, file.path(dada2_output_dir, "errF.rds"))
  saveRDS(errR, file.path(dada2_output_dir, "errR.rds"))
}else{
  errF <- readRDS(file.path(dada2_output_dir,"errF.rds"))
  errR <- readRDS(file.path(dada2_output_dir,"errR.rds"))
}



```{r, dada2 learn error rates plot}
plotErrors(errF, nominalQ=TRUE)
plotErrors(errR, nominalQ=TRUE)
```


## Sample inference

The dada function removes all sequencing errors to reveal the members of the sequenced community.

```{r dada2 sample inference}
if(!file.exists(file.path(dada2_output_dir,"dadaFs.rds")) && !file.exists(file.path(dada2_output_dir,"dadaRs.rds"))){
  dadaFs <- dada(filtFs, err=errF, multithread=params$threads)
  dadaRs <- dada(filtRs, err=errR, multithread=params$threads)
  saveRDS(dadaFs,file.path(dada2_output_dir,"dadaFs.rds"))
  saveRDS(dadaRs,file.path(dada2_output_dir,"dadaRs.rds"))
}else{
  dadaFs <- readRDS(file.path(dada2_output_dir,"dadaFs.rds"))
  dadaRs <- readRDS(file.path(dada2_output_dir,"dadaRs.rds"))
}

```

## Merge pairs

We now attempt to merge each denoised pair of forward and reverse reads.

```{r dada2 merge pairs, echo=TRUE, message=FALSE, eval=TRUE}
if (!file.exists(file.path(dada2_output_dir,"mergers.rds")) | !file.exists(file.path(dada2_output_dir,"seqtab.rds"))){
  if(!params$its == TRUE){
    mergers <- mergePairs(dadaFs, filtFs, dadaRs, filtRs, verbose=TRUE, minOverlap = params$merge_min_overlap, maxMismatch = params$merge_max_mismatch)
    seqtab <- makeSequenceTable(mergers)
  }else{
    mergers <- mergePairs(dadaFs, filtFs, dadaRs, filtRs, returnRejects = TRUE, trimOverhang = TRUE, verbose = TRUE, maxMismatch = params$merge_max_mismatch)
    concats <- mergePairs(dadaFs, filtFs, dadaRs, filtRs, justConcatenate=TRUE,verbose=TRUE)
    repair_merger <- function(merger, concat) {
     merger[!merger$accept, ] <- concat[!merger$accept, ]
     merger$sequence <- unlist(merger$sequence)
     return(merger)
    }
    repaired_mergers <- purrr::map2(mergers, concats, repair_merger)
    mergers <- repaired_mergers
    seqtab <- makeSequenceTable(mergers)
  }
  saveRDS(mergers,file.path(dada2_output_dir,"mergers.rds"))
  rownames(seqtab) <- sample.names <- unname(sapply(filtFs, get.sample.name))
  saveRDS(seqtab,file.path(dada2_output_dir,"seqtab.rds")) 
}else{
  mergers <- readRDS(file.path(dada2_output_dir,"mergers.rds"))
  seqtab <- readRDS(file.path(dada2_output_dir,"seqtab.rds"))
}

```


Finally, samples without remaining reads are removed: `r length(rownames(seqtab[rowSums(seqtab)==0,]))` sample(s) removed.

```{r}
if(length(rownames(seqtab[rowSums(seqtab)==0,]))>0){
	rownames(seqtab[rowSums(seqtab)==0,])
}
seqtab_final <- seqtab[rowSums(seqtab)>0,]
saveRDS(seqtab_final,file.path(dada2_output_dir,"seqtab.rds"))

```

We track reads through the pipeline:

```{r track reads, echo=FALSE}
track <- cbind(out, sapply(dadaFs, getN), sapply(dadaRs, getN), sapply(mergers, getN))
# If processing a single sample, remove the sapply calls: e.g. replace sapply(dadaFs, getN) with getN(dadaFs)
colnames(track) <- c("input", "filtered", "denoisedF", "denoisedR", "merged")
sample.names <- unname(sapply(fnFs, get.sample.name))
rownames(track) <- sample.names
saveRDS(track, file.path(dada2_output_dir, "track.rds"))
```

```{r datatable track, echo=FALSE}
track %>% datatable()
```


<div class="superbigimage">
```{r track reads plot, echo=FALSE, fig.width=max(8,ceiling(nrow(df_samples_raw)/2))}
my_levels <- c("input", "filtered", "denoisedF", "denoisedR", "merged")

p <- ggplot(as_tibble(track) %>% mutate(Sample=rownames(track))  %>% pivot_longer(cols = c(input, filtered, denoisedF, denoisedR, merged), names_to = "Step", values_to = "num_seqs") %>% mutate(Step = factor(Step, levels = my_levels)), 
       aes(x = Sample, y = num_seqs, fill=Step)) + 
  geom_bar(stat="identity", position=position_dodge())+
  scale_fill_brewer(palette="Paired")+
  theme(axis.text.x = element_blank(),
        axis.text.y = element_blank(), 
        axis.ticks.y = element_blank(), 
        strip.text.x = element_text(angle = 90)) +
  facet_wrap(~Sample, scales = "free_x", nrow = 1)
p
```
</div>

```{r frogs preparation}
frogs_output_dir <- file.path(params$output_directory, "FROGS")
if(!dir.exists(file.path(params$output_directory,"FROGS"))) dir.create(file.path(params$output_directory,"FROGS"))

seqtab <- readRDS(file.path(dada2_output_dir,"seqtab.rds"))

colnames(seqtab) <- gsub("NNNNNNNNNN", "NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN", colnames(seqtab))
    asv_seqs <- colnames(seqtab)
    asv_headers <- paste(">",colnames(seqtab),sep="")
    asv_headers <- if_else(grepl("NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN", asv_headers), paste0(">",asv_seqs, "_FROGS_combined"), asv_headers)
    asv_fasta <- c(rbind(asv_headers, asv_seqs))
    write(asv_fasta, file.path(dada2_output_dir,"dada2.fasta"))
    asv_tab <- t(seqtab)
    colnames(seqtab) <- if_else(grepl("NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN", colnames(seqtab)), paste0(colnames(seqtab), "_FROGS_combined"), colnames(seqtab))
    row.names(asv_tab) <- sub(">", "", asv_headers)
    write.table(asv_tab, file.path(dada2_output_dir,"dada2.tsv"), sep="\t", quote=F, col.names=NA)
    st.biom <- make_biom(t(seqtab))
    write_biom(st.biom, file.path(dada2_output_dir,"dada2.biom"))

```

# FROGS

Now we use FROGS.

## FROGS remove chimera

Chimera are removed.

```{r, eval=TRUE}
if(!file.exists(file.path(frogs_output_dir,"remove_chimera.biom")) | !file.exists(file.path(frogs_output_dir,"remove_chimera.fasta"))){
	system2(command="remove_chimera.py", args=c("--input-biom", file.path(dada2_output_dir,"dada2.biom"), "--input-fasta", file.path(dada2_output_dir,"dada2.fasta"), "--out-abundance", file.path(frogs_output_dir,"remove_chimera.biom"), "--summary", file.path(frogs_output_dir,"remove_chimera.html"), "--log-file", file.path(frogs_output_dir,"remove_chimera.log"), "--non-chimera", file.path(frogs_output_dir,"remove_chimera.fasta"), "--nb-cpus", params$threads))	
}

```

- [FROGS remove chimera report](FROGS/remove_chimera.html)

## FROGS filters

OTUs are filtered on abundance and phiX contamination:


```{r, eval=TRUE}
if(!file.exists(file.path(frogs_output_dir,"filters.biom")) | !file.exists(file.path(frogs_output_dir,"filters.fasta"))){
	system2(command="otu_filters.py", args=c("--input-biom", file.path(frogs_output_dir,"remove_chimera.biom"), "--input-fasta", file.path(frogs_output_dir,"remove_chimera.fasta"), "--output-biom", file.path(frogs_output_dir,"filters.biom"), "--summary", file.path(frogs_output_dir,"filters.html"), "--log-file", file.path(frogs_output_dir,"filters.log"), "--output-fasta", file.path(frogs_output_dir,"filters.fasta"), "--contaminant", params$phix, "--min-abundance", params$min_abundance, "--excluded", file.path(frogs_output_dir,"excluded.tsv"), "--nb-cpus", params$threads))	
}

```

- [FROGS filters report](FROGS/filters.html)

```{r itsx, results="asis"}
if(params$its == TRUE){
  cat("## FROGS ITSx\n")
  if(!file.exists(file.path(frogs_output_dir,"itsx.biom"))){
  system2(command="itsx.py", args = c("--nb-cpus", params$threads, "--region", params$its_region, "--input-biom", file.path(frogs_output_dir,"filters.biom"), "--out-removed" , file.path(frogs_output_dir,"itsx_removed.fasta"),  "--input-fasta", file.path(frogs_output_dir,"filters.fasta"), "--check-its-only" , "--out-abundance", file.path(frogs_output_dir,"itsx.biom"), "--out-fasta", file.path(frogs_output_dir,"itsx.fasta"), "--summary", file.path(frogs_output_dir, "itsx.html"), "--log-file", file.path(frogs_output_dir,"itsx.log")), stdout=TRUE) # input files
  }
  cat("\n")
  cat(" - [FROGS ITSx report](FROGS/itsx.html)")
}

```


## FROGS affiliation

Finally, OTUs are affiliated against a dedicated databank:

```{r}
if(!file.exists(file.path(frogs_output_dir,"affiliations.biom"))){
	if(params$its == TRUE){
		input_biom <- file.path(frogs_output_dir,"itsx.biom")
		input_fasta <- file.path(frogs_output_dir,"itsx.fasta")
	}else{
		input_biom <- file.path(frogs_output_dir,"filters.biom")
		input_fasta <- file.path(frogs_output_dir,"filters.fasta")
	}
	system2(command="affiliation_OTU.py", args=c("--input-biom", input_biom, "--input-fasta", input_fasta, "--output-biom", file.path(frogs_output_dir,"affiliations.biom"), "--summary", file.path(frogs_output_dir,"affiliation.html"), "--log-file", file.path(frogs_output_dir,"affiliation.log"), "--reference", params$reference, "--nb-cpus", params$threads))	
}

```

- [FROGS affiliation report](FROGS/affiliation.html)
- [FROGS BIOM file](FROGS/affiliations.biom)

## FROGS BIOM to TSV

```{r}
if(!file.exists(file.path(frogs_output_dir,"affiliations.tsv"))){
	system2(command="biom_to_tsv.py", args=c("--input-biom", file.path(frogs_output_dir,"affiliations.biom"), "--input-fasta", file.path(frogs_output_dir,"filters.fasta"), "--output-tsv", file.path(frogs_output_dir,"affiliations.tsv"), "--output-multi-affi", file.path(frogs_output_dir,"multi_affiliations.tsv"), "--log-file", file.path(frogs_output_dir,"biomtotsv.log")))	
}

```

- [FROGS tsv file](FROGS/affiliations.tsv)
- [FROGS multi affiliations file](FROGS/multi_affiliations.tsv)

The ASV table is available here:

```{r asv table, echo=FALSE}
counts <- read.csv2(file.path(params$output_directory,"FROGS/affiliations.tsv"), check.names = FALSE,  sep = "\t", header = TRUE) %>% as_tibble() #%>% select(-"comment")
counts %>%
  datatable(rownames = FALSE) 
```

# Reproductibility tokens

```{r, eval=TRUE}
system2(command="remove_chimera.py", args=c("--version"), stdout = TRUE)
system2(command="cutadapt", args=c("--version"), stdout = TRUE)
system2(command="seqkit", args=c("version"), stdout = TRUE)
```

```{r sessionInfo}
sessionInfo()
```

