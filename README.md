## Introduction

This repository contains the Rmd file used to perform a combination of dada2 and FROGS and generate a report. It is able to deal with 16S and ITS data.

Some adjustments are to be made to use it on another infrastructure than the migale platform.

### Workflow for 16S data

```mermaid
flowchart LR
    style FROGS stroke:#5f999d
    style dada2 stroke:#5f999d
    database[(Database)]-->affiliation
    fastq[[Fastq]]
    biom[[BIOM]]
    tsv[[TSV]]
    
    fastq-->seqkit
    seqkit---plotQualityProfiles
    subgraph dada2
        direction TB
        plotQualityProfiles-->filterAndTrim1[filterAndTrim]-->filterAndTrim2[filterAndTrim]-->learnErrors-->dada-->mergePairs-->makeSequenceTable
    end
    filterAndTrim1-->cutadapt-->filterAndTrim2
    makeSequenceTable-->rc
    subgraph FROGS
        direction TB
        rc[remove chimera]-->filters-->affiliation
    end
    
    
    affiliation-->biom
    affiliation--> tsv

```

### Workflow for ITS data

```mermaid
flowchart LR
    style FROGS stroke:#5f999d
    style dada2 stroke:#5f999d
    style mergePairs stroke:#5F999d,stroke-width:4px
    style ITSx stroke:#5F999d,stroke-width:4px
    database[(Database)]-->affiliation
    fastq[[Fastq]]
    biom[[BIOM]]
    tsv[[TSV]]
    
    fastq-->seqkit
    seqkit---plotQualityProfiles
    subgraph dada2
        direction TB
        plotQualityProfiles-->filterAndTrim1[filterAndTrim]-->filterAndTrim2[filterAndTrim]-->learnErrors-->dada-->mergePairs-->makeSequenceTable
    end
    filterAndTrim1-->cutadapt-->filterAndTrim2
    makeSequenceTable-->rc
    subgraph FROGS
        direction TB
        rc[remove chimera]-->filters-->ITSx-->affiliation
    end
    
    
    affiliation-->biom
    affiliation--> tsv

```

The documentation is available here: https://migale.pages.mia.inra.fr/dada2_frogs/

## Authors

- Olivier Rué

## License

 [![License: CC BY-SA 4.0](https://img.shields.io/badge/License-CC_BY--SA_4.0-lightgrey.svg)](https://creativecommons.org/licenses/by-sa/4.0/)

## Project status

This project is maintained and in progress
